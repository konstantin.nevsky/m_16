// m_16.cpp
//

#include <iostream>
#include <time.h>

int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);

    const int N = 5;
    int Array2D[N][N] = {};

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++) {
            Array2D[i][j] = i + j;
            std::cout << Array2D[i][j] << " ";
        }
        std::cout << "\n";
    }

    int RowIndex = buf.tm_mday % N;

    int Result = 0;

    for (int i = 0; i < N; i++)
    {
        Result += Array2D[RowIndex][i];
    }


    std::cout << "\nSum: " << Result << "\n";
}

